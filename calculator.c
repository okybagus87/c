#include <stdio.h>
#include <stdlib.h>

int main()
{
    char op1, op2;
    float angka1, angka2, angka3, hasil;

    printf("Masukkan ekspresi bilangan (contoh: 3*4+2): ");
    scanf("%f %c %f %c %f", &angka1, &op1, &angka2, &op2, &angka3);

    if (op1 == '*')
    {
        switch (op2)
        {
        case '*':
            hasil = angka1 * angka2 * angka3;
            break;
        case '/':
            if (angka3 == 0)
            {
                printf("Bilangan tidak bisa dibagi 0");
                return 0;
            }
            else
            {
                hasil = angka1 * angka2 / angka3;
            }
            break;
        case '+':
            hasil = angka1 * angka2 + angka3;
            break;
        case '-':
            hasil = angka1 * angka2 - angka3;
            break;

        default:
            break;
        }
    }
    else if (op1 == '/')
    {
        if (angka2 == 0)
        {
            printf("Bilangan tidak bisa dibagi 0");
            return 0;
        }
        else
        {
            switch (op2)
            {
            case '*':
                hasil = angka1 / angka2 * angka3;
                break;
            case '/':
                if (angka3 == 0)
                {
                    printf("Bilangan tidak bisa dibagi 0");
                    return 0;
                }
                else
                {
                    hasil = angka1 / angka2 / angka3;
                }
                break;
            case '+':
                hasil = angka1 / angka2 + angka3;
                break;
            case '-':
                hasil = angka1 / angka2 - angka3;
                break;

            default:
                break;
            }
        }
    }
    else if (op1 == '+')
    {
        switch (op2)
        {
        case '*':
            hasil = angka1 + (angka2 * angka3);
            break;
        case '/':
            if (angka3 == 0)
            {
                printf("Bilangan tidak bisa dibagi 0");
                return 0;
            }
            else
            {
                hasil = angka1 + (angka2 / angka3);
            }
            break;
        case '+':
            hasil = angka1 + angka2 + angka3;
            break;
        case '-':
            hasil = angka1 + angka2 - angka3;
            break;

        default:
            break;
        }
    }
    else if (op1 == '-')
    {
        switch (op2)
        {
        case '*':
            hasil = angka1 - (angka2 * angka3);
            break;
        case '/':
            if (angka3 == 0)
            {
                printf("Bilangan tidak bisa dibagi 0");
                return 0;
            }
            else
            {
                hasil = angka1 - (angka2 / angka3);
            }
            break;
        case '+':
            hasil = angka1 - angka2 + angka3;
            break;
        case '-':
            hasil = angka1 - angka2 - angka3;
            break;

        default:
            break;
        }
    }

    printf("Hasil %.2f %c %.2f %c %.2f = %.2f\n", angka1, op1, angka2, op2, angka3, hasil);
    return 0;
}